import express, { json } from "express"
import firstRouter from "./src/routes/firstRouter.js"
import secondRouter from "./src/routes/secondRouter.js"
import mongoose from "mongoose"
import connectToMongoDb from "./src/routes/connectToDatabase/connectToMongoDB.js"
import { studentRouter } from "./src/routes/studentRouter.js"
import { teacherRouter } from "./src/routes/teacherRouter.js"
//make express application

let expressApp = express()
// expressApp.use((req,res,next)=>{
//     console.log("i am app middleware 0")
//     next()
// },(req,res,next)=>{
//     console.log("i am app middleware 1")
//     next()
// })

expressApp.use(json())
//attach port to the express application instance

expressApp.listen(8000, () =>{
    console.log("app is listening at port 8000")
})

expressApp.use("/",firstRouter)//localhost:8000
expressApp.use("/seconds",secondRouter)//localhost:8000
expressApp.use("/students",studentRouter)//localhost:8000
expressApp.use("/teachers",teacherRouter )//localhost:8000



//make api
//defining task for each request

//request
//url,request

//define Router
//use  that Router to expressApp






connectToMongoDb()