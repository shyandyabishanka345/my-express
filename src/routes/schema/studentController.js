



export let deleteStudent = async (req, res) => {
    let id = req.params.id;
  
    try {
      let result = await Student.findByIdAndDelete(id);
  
      res.json({
        success: true,
        message: "Student deleted successfully.",
        data: result,
      });
    } catch (error) {
      res.json({
        success: false,
        message: error.message,
      });
    }
  };