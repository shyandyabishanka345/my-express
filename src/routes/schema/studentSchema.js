import { Router } from "express";
  export let studentRouter = Router()




studentRouter
.route("/")
.post ((req,res) =>{

    
    res.json({
        success: true,
        message: "students creates succesfully.",
    })
})

.get((req,res) =>{
    res.json({

        success: true,
        message: "students read succesfully.",
    })
})

.patch((req,res)=>{
    res.json({

        success: true,
        message: "students updated succesfully.",
    })
})

.delete((req,res) =>{
    res.json({
        success: true,
        message: "students deleted succesfully.",

    })
})



import { Schema } from "mongoose";

let studentSchema = Schema({
    name :{
        type:String,
        required:[true,"name field is required"],
    },

    age :{
        type:Number,
        required:[true,"age field is required"],
    },
    isMarried :{
        type:Boolean,
        required:[false],
    },


})

export default studentSchema

