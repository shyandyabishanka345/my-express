import mongoose from "mongoose";
let connectToMongoDb = async () =>{
    try{
        await mongoose.connect ("mongodb://0.0.0.0:27017")
       console.log("application is connected to the database successfully")
   
   }
   catch(error) {
       console.log("unable to connect database")
   }
}

export default connectToMongoDb