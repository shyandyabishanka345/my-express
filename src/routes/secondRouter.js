//define router   
import {Router} from "express"
let secondRouter = Router()

//middleware
//It is a function which has req,res,next
//next function is used to trigger another middleware
//for one request we must have only one  response
//it is divided into two category

//Based on error
//error middleware
//(err,req,res,next)={}
//next (value)

//without error middleware
//(req,res,next)=>{}
//to trigger next middleware next()

//Based on location
//route middleware
//application middleware




secondRouter
.route("/")//localhost:8000/seconds
.post ((req,res,next)=>{
    console.log("i am middleware 1")
    res.json("middleware1")
    next()
},(req,res,next)=>{
    console.log("i am middleware 2")
    
    next()

},(req,res,next)=>{ 
    console.log("i am middleware 3")

})


.get((req,res) =>{
    res.json("home get")
})

.patch((req,res)=>{
    res.json("home patch")
})

.delete((req,res) =>{
    res.json("home delete")
})

secondRouter
.route("/")//localhost:8000/name
.post ((req,res) =>{
    res.json ("name post")
})


.get((req,res) =>{
    res.json("name get")
})

.patch((req,res)=>{
    res.json("name patch")
})

.delete((req,res) =>{
    res.json("name delete")
})



secondRouter
.route("/")//localhost:8000/seconds
.post ((req,res) =>{

    //console.log(req.body)
    console.log("hello")

})


.get((req,res) =>{
    res.json("home get")
})

.patch((req,res)=>{
    res.json("home patch")
})

.delete((req,res) =>{
    res.json("home delete")
})

secondRouter
.route("/ab")//localhost:8000/seconds/ab
.post ((req,res) =>{
    res.json ("name post")
})


.get((req,res,next)=>{
    console.log("middleware 0")
    next("ram")
},( err,req,res,next)=>{
    console.log("i am middleware 1")
    next()
},

(req,res,next)=>{
    console.log("i am middleware 2")
    
})

.patch((req,res)=>{
    res.json("name patch")
})

.delete((req,res) =>{
    res.json("name delete")
})





export default secondRouter